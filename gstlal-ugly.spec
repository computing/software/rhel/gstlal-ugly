%define gstreamername gstreamer1
%global __python %{__python3}

Name: gstlal-ugly
Version: 1.10.0
Release: 1.2%{?dist}
Summary: GSTLAL Experimental Supplements
License: GPL
Group: LSC Software/Data Analysis

# --- package requirements --- #
Requires: gstlal >= 1.10.0
Requires: gobject-introspection >= 1.30.0
Requires: fftw >= 3
Requires: %{gstreamername} >= 1.14.1
Requires: %{gstreamername}-plugins-base >= 1.14.1
Requires: %{gstreamername}-plugins-good >= 1.14.1
Requires: %{gstreamername}-plugins-bad-free
Requires: python%{python3_pkgversion}-%{gstreamername}
Requires: orc >= 0.4.16
Requires: gsl

# --- python package requirements --- #
Requires: python%{python3_pkgversion} >= 3.6
Requires: python%{python3_pkgversion}-confluent-kafka
Requires: python%{python3_pkgversion}-h5py
Requires: python%{python3_pkgversion}-numpy >= 1.7.0
Requires: python%{python3_pkgversion}-scipy

%if 0%{?rhel} == 8
Requires: python%{python3_pkgversion}-numpy >= 1.7.0
Requires: python3-matplotlib
%else
Requires: numpy >= 1.7.0
Requires: python%{python3_pkgversion}-matplotlib
%endif

# --- LSCSoft package requirements --- #
Requires: lal >= 7.2.4
Requires: lalmetaio >= 3.0.2
Requires: ldas-tools-framecpp >= 2.6.2
Requires: gds-lsmp >= 2.19.0
Requires: gds-framexmit >= 2.19.0
Requires: nds2-client >= 0.11.5
Requires: python%{python3_pkgversion}-ligo-lw >= 1.8.3
Requires: python%{python3_pkgversion}-ligo-segments >= 1.2.0
Requires: python%{python3_pkgversion}-ligo-scald >= 0.7.2

# -- build requirements --- #
BuildRequires: pkgconfig >= 0.18.0
BuildRequires: doxygen  >= 1.8.3
BuildRequires: graphviz
BuildRequires: gstlal-devel >= 1.10.0
BuildRequires: python3-devel >= 3.6
BuildRequires: fftw-devel >= 3
BuildRequires: gobject-introspection-devel >= 1.30.0
BuildRequires: %{gstreamername}-devel >= 1.14.1
BuildRequires: %{gstreamername}-plugins-base-devel >= 1.14.1
BuildRequires: liblal-devel >= 7.2.4
BuildRequires: liblalmetaio-devel >= 3.0.2
BuildRequires: gsl-devel
BuildRequires: gtk-doc >= 1.11
BuildRequires: ldas-tools-framecpp-devel >= 2.6.2
BuildRequires: gds-lsmp-devel >= 2.19.0
BuildRequires: gds-framexmit-devel >= 2.19.0
BuildRequires: nds2-client-devel >= 0.11.5
BuildRequires: nds2-client-headers >= 0.11.5
BuildRequires: orc >= 0.4.16
Source: https://software.igwn.org/lscsoft/source/gstlal-ugly-%{version}.tar.gz
URL: https://git.ligo.org/lscsoft/gstlal
Packager: Kipp Cannon <kipp.cannon@ligo.org>
BuildRoot: %{_tmppath}/%{name}-%{version}-root
%description
This package provides a variety of gstreamer elements for
gravitational-wave data analysis and some libraries to help write such
elements.  The code here sits on top of several other libraries, notably
the LIGO Algorithm Library (LAL), FFTW, the GNU Scientific Library (GSL),
and, of course, GStreamer.

This package contains the plugins and shared libraries required to run
gstlal-based applications.


%package devel
Summary: Files and documentation needed for compiling gstlal-based plugins and programs.
Group: LSC Software/Data Analysis
Requires: gstlal-devel >= 1.10.0 
Requires: python3-devel >= 3.6 
Requires: fftw-devel >= 3 
Requires: %{gstreamername}-devel >= 1.14.1 
Requires: %{gstreamername}-plugins-base-devel >= 1.14.1 
Requires: liblal-devel >= 7.2.4 
Requires: liblalmetaio-devel >= 3.0.2 
Requires: gsl-devel 
Requires: nds2-client-headers >= 0.11.5
%description devel
This package contains the files needed for building gstlal-ugly based
plugins and programs.


%prep
%setup -q -n %{name}-%{version}


%build
%configure --enable-gtk-doc PYTHON=python3
%{__make}


%install
# FIXME:  why doesn't % makeinstall macro work?
DESTDIR=${RPM_BUILD_ROOT} %{__make} install
# remove .so symlinks from libdir.  these are not included in the .rpm,
# they will be installed by ldconfig in the post-install script, except for
# the .so symlink which isn't created by ldconfig and gets shipped in the
# devel package
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT}/%{_libdir} -name "*.so.*" -type l -delete
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete


%post
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%postun
if test -d /usr/lib64 ; then
	ldconfig /usr/lib64
else
	ldconfig
fi


%clean
[ ${RPM_BUILD_ROOT} != "/" ] && rm -Rf ${RPM_BUILD_ROOT}
rm -Rf ${RPM_BUILD_DIR}/%{name}-%{version}


%files
%defattr(-,root,root)
%{_bindir}/*
%{_libdir}/gstreamer-1.0/*.so
%{_prefix}/%{_lib}/python*/site-packages/gstlal

%files devel
%defattr(-,root,root)
%{_libdir}/gstreamer-1.0/*.a
